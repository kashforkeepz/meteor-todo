 import { Template } from 'meteor/templating';
 import { ReactiveVar } from 'meteor/reactive-var';

 import './main.html';

Todos = new Mongo.Collection("todos");

TodoSchema = new SimpleSchema({
	label: {
		type: String,
		max: 200
	},
	createdAt: {
		type: Date,
		defaultValue: new Date()
	}
});

Todos.attachSchema(TodoSchema);

if (Meteor.isClient) {

Session.set("sortOrder", 1);

Template.todosList.helpers({
  todos: function () {
  	return Todos.find({}, {sort: {createdAt: Session.get("sortOrder")}});
  }
});

Template.todosList.events({
	"click #reverse-sort": function () {
			Session.set("sortOrder", Session.get("sortOrder") * -1);
		}	
	});

Template.todo.events({
	"click input": function () {
		var isDone = Todos.findOne({_id: this._id}).done;
		Todos.update({_id: this._id}, {$set: {done: !isDone}});
		},
		"click .delete": function () {
			Todos.remove({_id: this._id});
		}
	});

Template.todo.helpers({
	done: function () {
		return Todos.findOne({_id: this._id}).done;
	}
	});
}